var xmlHttp;

function createXMLHttpRequest() {
    if (window.ActiveXObject) {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    else if (window.XMLHttpRequest) {
        xmlHttp = new XMLHttpRequest();
    }
}

function startRequest() {
    createXMLHttpRequest();
    try {
        xmlHttp.onreadystatechange = handleStateChange;
        xmlHttp.open("GET", "test.txt", true);
        xmlHttp.send(null);
    } catch (exception) {
        alert("您要访问的资源不存在!");
    }
}

function handleStateChange() {
    if (xmlHttp.readyState == 4) {
        if (xmlHttp.status == 200 || xmlHttp.status == 0) {
            // 显示返回结果
            document.getElementById("test").innerHTML = xmlHttp.responseText;
        }
    }
}
$(function () {
    $('#btn1').click(function () {
        startRequest()
    });

});
