function Flt(fltNo, bkd, cap, deptAirport, arrAirport) {
    this.fltNo = fltNo;
    this.bkd = bkd;
    this.cap = cap;
    this.deptAirport = deptAirport;
    this.arrAirport = arrAirport;
    this.setLf = function (lf) {
        this.lf = lf;
    }
    this.getLf = function () {
        return this.lf;
    }
    this.getSeg = function () {
        return this.deptAirport + this.arrAirport;
    }
}

function FltWarningRule(seg, segLf) {
    this.seg = seg;
    this.segLf = segLf;
    this.fltLfIsNormal=function(flt){
        if(flt.getSeg()===this.seg){
            if(flt.getLf()<this.segLf){
                console.log('warning! flt:ca1501 lf:0.75 is under segLf!');
            }
        }
    }
}
var flt = new Flt('a','b','c','d','e');
flt.setLf(0.5);
var seg=flt.getSeg();
console.log(seg,flt.getLf());
var fltWarningRule=new FltWarningRule('de',0.6)
fltWarningRule.fltLfIsNormal(flt);