var startDate = '2016-1-1',//开始日期,如：'2016-1-1'
    endDate = '2016-2-31',//结束日期,如：'2016-2-31'
    frequency = '1356',//班期（星期）,如：'1356'
    leg = ['pek', 'ckg', 'sha'];//航段,如：[‘pek’, ‘ckg’, ‘sha’]
function formatDate(time) {
    var date = new Date(time);
    var year = date.getFullYear(),
        month = date.getMonth() + 1,
        day = date.getDay();
    var newTime = year + (month < 10 ? '0' + month : month) + (day < 10 ? '0' + day : day);
    return newTime;
}
function buildRequestData() {
    var arg1 = arguments[0];
    var arg2 = arguments[1];
    var arg3 = arguments[2];
    var arg4 = arguments[3];
    if (arguments.length === 4) {
        return [arg1, arg2, arg3, arg4].join('#');
    } else if (arguments.length === 3) {
        return [arg1, arg2, arg3].join('-');
    } else {
        return;
    }
}
function getUnShiftFrequency(str) {
    let strarr = str.split('');
    let source = ['1', '2', '3', '4', '5', '6', '7'];
    var result = [];
    for (let i = 0; i < source.length; i++) {
        if (!strarr.includes(source[i])) {
            result.push(source[i])
        }
    }
    return result.join('');
}
function buildRequireData(startDate, endDate, frequency, leg) {
    let newStartDate = formatDate(startDate),
        newEndDate = formatDate(endDate),
        UnShiftFrequency = getUnShiftFrequency(frequency);
    let legResult = function () {
        let res = leg.join('-').toUpperCase();
        return res;
    }();
    return buildRequestData(newStartDate, newEndDate, UnShiftFrequency, legResult)
}
let result = buildRequireData(startDate, endDate, frequency, leg);
console.log(result);