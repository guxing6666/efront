// function func() {
//     var arr = [];
//     for(let i = 0;i<3;i++){
//         arr.push(()=> {
//             console.log(i);
//         })
//     }
//     return arr
// }
// var result = func();
// result.forEach((item)=> {
//     item();
// })
// function func() {
//     var arr = [];
//     for(var i = 0;i<3;i++){
//         (function(i){
//             arr.push((i)=>{
//                 console.log(i)
//             })
//         })(i)
//     }
//     return arr
// }
// var result = func();
// result.forEach((item)=> {
//     item();
// })
// var scope = "global scope";
// function checkscope(){
//     var scope = "local scope";
//     function f(){
//         return scope;
//     }
//     return f();
// }
var scope = "global scope";
function checkscope(){
    var scope = "local scope";
    function f(){
        return scope;
    }
    return f;
}
let result=checkscope()();
console.log(result)